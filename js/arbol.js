class arbol{
    constructor(){
        this.nodoPadre = "";
        this.nodos = [];
    }


    agregarNodoPadre(nodoPadre, posicion, nombre, valor, nivel){
        var nodo = new Nodo(null,null,nombre,valor,0);
        this.nodos.push(nodo);
        return nodo;
    }

    agregarNodo(valor, nombre){
        let nodo = new Nodo(null, null, nombre, valor, null);
        if (this.nodos.length == 1){
            nodo.padre = this.nodoPadre;
            nodo.nivel = this.nodoPadre.nivel +1;
            if(nodo.valor < this.nodoPadre.valor){
                nodo.posicion = 'HI';
            }else{
                nodo.posicion = 'HD';
            }
        }else if(this.nodos.length == 2){
            nodo.padre = this.nodoPadre;
            nodo.nivel = 1;
            if(nodo.valor < this.nodoPadre.valor){
                nodo.posicion = 'HI';
            }else{
                nodo.posicion = 'HD';
            }
        }
        this.nodos.push(nodo);
    }
}